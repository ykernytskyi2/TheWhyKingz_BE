#Docker Instruction

### Change global git settings (For windows)
~~~
git config --global core.autocrlf input
~~~

### First run after clone
- sudo apt install make
- cp .env.exapmle .env (And set config for this file)
- make run run-and-import-remote-db

### Every day
- make run (start containers)
- make restart (restart)
- make down (stop containers)

### Commands
- cd {to/root/of/project}
- docker-compose build
- docker-compose up -d 
