#!/usr/bin/env bash

cd /var/www/

chmod 0777 -R ./storage

composer install
sleep 5

php artisan key:generate
php artisan migrate
php artisan db:seed
php artisan optimize:clear
