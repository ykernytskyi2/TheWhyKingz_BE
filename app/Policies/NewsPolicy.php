<?php

namespace App\Policies;

use App\Models\News;
use App\Models\User;

class NewsPolicy
{
    public function view(?User $user, News $news): bool
    {
        return $news->is_active;
    }

    /**
     * Determine whether the user can update the model.
     */
    public function hide(?User $user, News $news): bool
    {
        return $news->is_active;
    }
}
