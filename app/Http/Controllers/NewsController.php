<?php

namespace App\Http\Controllers;

use App\Http\Resources\News\NewsForListResource;
use App\Http\Resources\News\SingleNewsResource;
use App\Models\News;
use App\Service\News\NewsService;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response;

class NewsController extends Controller
{
    protected NewsService $newsService;

    public function __construct(NewsService $newsService)
    {
        $this->newsService = $newsService;
    }

    public function index(): AnonymousResourceCollection
    {
        $news = $this->newsService->all();

        return NewsForListResource::collection($news);
    }

    public function show(News $news): SingleNewsResource
    {
        return SingleNewsResource::make($news);
    }

    public function hide(News $news):Response
    {
        $this->newsService->hide($news);

        return response()->noContent();
    }

}
