<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

/**
 * @property integer $id
 * @property string $title
 * @property string $slug
 * @property string $short_description
 * @property string $description
 * @property integer|bool $is_active
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class News extends Model
{
    use HasFactory;

    protected $attributes = [
        'title' => '',
        'description' => '',
        'short_description' => '',
        'is_active' => false,
        'slug' => '',
    ];

    protected $fillable = [
        'title',
        'description',
        'short_description',
        'is_active'
    ];

    protected $casts = [
        'is_active' => 'boolean',
    ];

    protected $appends = ['url'];

    /*
     * Mutators & Casting
     */

    protected function url(): Attribute
    {
        return Attribute::make(
            get: fn () => config('app.client_url') . "/{$this->slug}",
        );
    }

    protected function title(): Attribute
    {
        return Attribute::make(
            get: fn (string $value) => $value,
            set: fn ($value) => [
                'title' => $value,
                'slug' => Str::slug($value),
            ],
        );
    }

    /*
     * Scopes
     */
    public function scopeActive(Builder $query): Builder
    {
        return $query->where('is_active', true);
    }
}
