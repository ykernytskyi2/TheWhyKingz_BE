<?php

namespace App\Service\News;

use App\Models\News;

class NewsService
{
    public function all()
    {
        return News::query()
            ->select(['title', 'slug', 'short_description', 'created_at', 'updated_at'])
            ->active()
            ->get();
    }

    public function hide(News $news): bool
    {
        return $news
            ->fill(['is_active' => false])
            ->save();
    }

}
