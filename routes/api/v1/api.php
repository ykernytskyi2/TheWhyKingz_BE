<?php

use App\Http\Controllers\NewsController;
use Illuminate\Support\Facades\Route;

Route::prefix('news')->group(function () {
    Route::get('/', [NewsController::class, 'index']);
    Route::get('/{news:slug}', [NewsController::class, 'show'])
        ->can('view', 'news');
    Route::put('/{news:slug}', [NewsController::class, 'hide'])
        ->can('hide', 'news');
});
