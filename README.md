## Run app by docker

- Setup .env (cp .env.example .env)
- docker-compose build
- docker-compose up -d
- docker exec -it TheWhyKingz_app /bin/sh ./docker/containers/app/init.sh
- http://loclhost:8000
